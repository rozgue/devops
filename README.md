# README #

This README would normally document whatever steps are necessary to get your application up and running.


### What is this repository for? ###

* Repo for devops project environment automation. 
* 1.0
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

You must to have a remote server image to do connection with ssh service and downloades sshkeyfile. 
In this case is devEnv.pem defined into hosts file

* 1.-Install ansible
* 2.-Download code from repo
*    NOTE: To define ssh access firts you need to create a git folder project into local server
         * after this. set remote origin master "$git remote add origin master"
         * once you set origin then set url origin "$git remote set-url origin https://rozgue@bitbucket.org/rozgue/devops.git"
         * there is a trouble if you wish do this with ssh
         * before all you need a public and private key loaded in the local server and publicated this public key into bitbucket
* 3.-Define host ip, user, and ssh credentials server(keyfile), all this into hosts file defined on root repo path
* 4.- downloaded files into /opt directory... yuo must to download versions and tools defined 
*   into playbook.yml to copy from /opt/toolToCopy to remote server. All this is defined into compy modules of playbook.yml file 
* 5.-run playbook.yml
* $"ansible-playbook playbook.yml"
 
 
### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo admin
* Other community or team contact